import Vue from 'vue'
import Router from 'vue-router'
import Home from '../views/Home.vue'
import About from '../views/About.vue'
import Videos from '../views/Videos.vue'
import External from '../views/ExternalLinks.vue'
import Table from '../views/Table.vue'

Vue.use(Router)


export const router = new Router({
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home
    },
    {
      path: '/about',
      name: 'about',
      component: About
    },
    {
      path: '/videos',
      name: 'videos',
      component: Videos
    },
    {
      path: '/external',
      name: 'external',
      component: External
    },
    {
      path: '/table',
      name: 'table',
      component: Table
    },

    // otherwise redirect to home
    { path: '*', redirect: '/' }
  ]
})
