import Vue from 'vue'
import App from './App.vue'
import { store } from './_store';
import { router } from './_helpers';

Vue.config.productionTip = false

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
